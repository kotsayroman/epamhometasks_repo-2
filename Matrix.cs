using System;

namespace EpamHomeTasks
{
    class Matrix
    {
        //PRIVATE FIELDS
        //private field for matrix
        private int[,] _matrix;
        
        //private fields for rows and columns
        private int _rows;
        private int _columns;
        
        //Constructor
        public Matrix(int rows, int columns)
        {
            if(rows > 0 && columns > 0)
            {
                this._rows = rows;
                this._columns = columns;
                _matrix = new int[_rows, _columns];
            }
            else
            {
                throw new Exception("Rows and Columns must be > 0");
            }
        }
        
        //PUBLIC PROPERTIES
        //public properties for _rows and _columns
        public int Rows
        {
            get 
            {
                return this._rows;
            }
        }
        
        public int Columns
        {
            get
            {
                return this._columns;
            }
        }
        
        //public property which return number of all elements in matrix
        public int Length
        {
            get
            {
                return this._matrix.Length;
            }
        }
        
        //indexer for Matrix class
        public int this[int rowIndex, int colIndex]
        {
            get
            {
                if((rowIndex >= 0 && rowIndex < this._rows) && (colIndex >= 0 && colIndex < this._columns))
                {
                    return this._matrix[rowIndex, colIndex];
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
            set
            {
                if((rowIndex >= 0 && rowIndex < this._rows) && (colIndex >= 0 && colIndex < this._columns))
                {
                    this._matrix[rowIndex, colIndex] = value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }
        
        //PUBLIC METHODS
        //method which fill matrix by random values
        public void Fill(int minValue, int maxValue)
        {
            Random rand = new Random();
            
            for(int i = 0; i < this._rows; i++)
            {
                for(int j = 0; j < this._columns; j++)
                {
                    this._matrix[i, j] = rand.Next(minValue, maxValue + 1);
                }
            }
        }
        
        public override string ToString()
        {
            string result = "";
            
            for(int i = 0; i < this._rows; i++)
            {
                for(int j = 0; j < this._columns; j++)
                {
                    result += string.Format("{0} ", this[i, j]);
                }
                result += "\n\n";
            }
            
            return result;
        }
        
        public override bool Equals(object o)
        {
            if(o is Matrix)
            {
                return this == (Matrix)o;
            }
            else
            {
                return false;
            }
        }
        
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
        //static method to add two matrixes
        public static Matrix Add(Matrix m1, Matrix m2)
        {
            if(m1.Rows == m2.Rows && m1.Columns == m2.Columns)
            {
                Matrix result = new Matrix(m1.Rows, m2.Columns);
                for(int i = 0; i < m1.Rows; i++)
                {
                    for(int j = 0; j < m2.Columns; j++)
                    {
                        result[i, j] = m1[i, j] + m2[i, j];
                    }
                }
                return result;
            }
            else
            {
                throw new Exception("Martrix must have single sizes!");
            }
        }
        
        //static method to substract one matric from another
        public static Matrix Substract(Matrix m1, Matrix m2)
        {
            if(m1.Rows == m2.Rows && m1.Columns == m2.Columns)
            {
                Matrix result = new Matrix(m1.Rows, m2.Columns);
                for(int i = 0; i < m1.Rows; i++)
                {
                    for(int j = 0; j < m2.Columns; j++)
                    {
                        result[i, j] = m1[i, j] - m2[i, j];
                    }
                }
                return result;
            }
            else
            {
                throw new Exception("You can substract one matrix from anotner only if they have simular size");
            }
        }
        
       //static method to multiply two matrixes
        public static Matrix Multiply(Matrix  m1, Matrix m2)
        {
            if(m1.Columns == m2.Rows)
            {
                Matrix result = new Matrix(m1.Columns, m2.Rows);
                for(int i = 0; i < m1.Rows; i++)
                {
                    for(int k = 0; k < m2.Columns; k++)
                    {
                        for(int j = 0; j < m1.Columns; j++)
                        {
                            result[i, k] += m1[i, j] * m2[j, k];
                        }
                    }
                }
                return result;
            }
            else
            {
                throw new Exception("Columns of first matrix must be equals to rows of a second matrix");
            }
        }
        
        //static method to multiply matrix by a number
        public static Matrix Multiply(Matrix m1, int scalar)
        {
            Matrix result = new Matrix(m1.Rows, m1.Columns);
            for(int i = 0; i < m1.Rows; i++)
            {
                for(int j = 0; j < m1.Columns; j++)
                {
                    result[i, j] = m1[i, j] * scalar;
                }
            }
            return result;
        }
        
        //static method to transponate the matrix
        public static Matrix Transp(Matrix m)
        {
            Matrix result = new Matrix(m.Columns, m.Rows);
            for(int i = 0; i < m.Columns; i++)
            {
                for(int j = 0; j < m.Rows; j++)
                {
                    result[i, j] = m[j, i];
                }
            }
            return result;
        }
        
        //static method to get a submatrix with a given size
        public static Matrix SubMatrix(Matrix m, int rows, int columns)
        {
            if(rows < m.Rows && columns < m.Columns && rows > 0 && columns > 0)
            {
                Matrix result = new Matrix(rows, columns);
                for(int i = 0; i < rows; i++)
                {
                    for(int j = 0; j < columns; j++)
                    {
                        result[i, j] = m[i, j];
                    }
                }
                return result;
            }
            else
            {
                throw new Exception("Wrong number of rows and columns of submatrix");
            }
        }
        
        
        
        //OVERLOADED OPERATORS
        //overloaded operator + to add two matrixes
        public static Matrix operator + (Matrix m1, Matrix m2)
        {
            return Matrix.Add(m1, m2);
        }
        
        //overloaded operator - to substract one matrix from anoher
        public static Matrix operator - (Matrix m1, Matrix m2)
        {
            return Matrix.Substract(m1, m2);
        }
        
        //overloaded operator * to multiply two matrixes
        public static Matrix operator * (Matrix m1, Matrix m2)
        {
            return Matrix.Multiply(m1, m2);
        }
   
        //overloaded operator * to multiply matrix by scalar
        public static Matrix operator * (Matrix m, int number)
        {
            return Matrix.Multiply(m, number);
        }
        
        public static bool operator == (Matrix m1, Matrix m2)
        {
            bool IsEquals = true;
            if(m1.Rows == m2.Rows && m1.Columns == m2.Columns)
            {
                for(int i = 0; i < m1.Rows; i++)
                {
                    if(!IsEquals)
                    {
                        break;
                    }
                    for(int j = 0; j < m2.Columns; j++)
                    {
                        if(m1[i, j] != m2[i, j])
                        {
                            IsEquals = false;
                        }
                    }
                }
            }
            return IsEquals;
        }
        
        public static bool operator != (Matrix m1, Matrix m2)
        {
            bool IsEquals = false;
            if(m1.Rows == m2.Rows && m1.Columns == m2.Columns)
            {
                for(int i = 0; i < m1.Rows; i++)
                {
                    if(IsEquals)
                    {
                        break;
                    }
                    for(int j = 0; j < m2.Columns; j++)
                    {
                        if(m1[i, j] != m2[i, j])
                        {
                            IsEquals = true;
                        }
                    }
                }
            }
            return IsEquals;
        }
        
    }
    
    class Program
    {
        static void Main()
        {
            try
            {
                Matrix first_matrix = new Matrix(4, 4);
                first_matrix.Fill(5, 40);
                Console.WriteLine("First Matrix:\n{0}", first_matrix);
                
                Matrix second_matrix = new Matrix(4, 4);
                second_matrix.Fill(2, 10);
                Console.WriteLine("Second Matrix:\n{0}", second_matrix);
                
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("ADDITION:");
                Console.WriteLine("First Matrix + Second Matrix:\n{0}", first_matrix + second_matrix);                
                Console.WriteLine("Now we are using static method Add: result is the same:\n{0}", Matrix.Add(first_matrix, second_matrix));           
                
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("SUBSTRACTION:");
                Console.WriteLine("First Matrix - Second Matrix:\n{0}", first_matrix - second_matrix);         
                Console.WriteLine("Now we are using static method Substract: result is the same:\n{0}", Matrix.Substract(first_matrix, second_matrix));
                
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("MULTIPLICATION:");
                Console.WriteLine("First Matrix * Second Matrix:\n{0}", first_matrix * second_matrix);          
                Console.WriteLine("Now we are using static method Multiply: result is the same:\n{0}", Matrix.Multiply(first_matrix, second_matrix));                      
                Console.WriteLine("First Matrix * 10:\n{0}", first_matrix * 10);          
                Console.WriteLine("Now we are using static method Multiply: result is the same:\n{0}", Matrix.Multiply(first_matrix, 10));       
                
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("TANSPOSITION:");
                Console.WriteLine("Transponate second matrix:\n{0}", Matrix.Transp(second_matrix));
                
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("SUBMATRIX:");
                Console.WriteLine("Submatrix with a given size:\n{0}", Matrix.SubMatrix(first_matrix, 2, 2));
    
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("COMPARISON:");
                Console.WriteLine("first_matrix == matrix2 = {0}", first_matrix == second_matrix);
                Console.WriteLine("first_matrix != matrix2 = {0}", first_matrix != second_matrix);
                Console.WriteLine("first_matrix == first_matrix = {0}", first_matrix == first_matrix);
                Console.WriteLine("first_matrix != first_matrix = {0}", first_matrix != first_matrix);
                Console.WriteLine("first_matrix.Equals(matrix2) = {0}", first_matrix.Equals(second_matrix));
                Console.WriteLine("first_matrix.Equals(first_matrix) = {0}", first_matrix.Equals(first_matrix));
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }
    }
}